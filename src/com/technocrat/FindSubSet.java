package com.technocrat;

public class FindSubSet {

    // Print all subsets of given set[]
    static void printSubsets() {
        FasterScanner fasterScanner = new FasterScanner ();

        int noOfTestCases = fasterScanner.nextInt ();

        for (int i = 0; i < noOfTestCases; i++) {
            int noOfRecords = fasterScanner.nextInt ();
            int[] records = new int[noOfRecords];

            for (int j = 0; j < noOfRecords; j++) {
                records[j] = fasterScanner.nextInt ();
            }

            int n = records.length;

            // Run a loop for printing all 2^n
            // subsets one by one

            for (int k = 0; k < (1 << n); k++) {
                int previous = 0;
                // Print current subset
                for (int l = 0; l < n; l++)

                    // (1<<j) is a number with jth bit 1
                    // so when we 'and' them with the
                    // subset number we get which numbers
                    // are present in the subset and which
                    // are not
                    if ((k & (1 << l)) > 0)
                        previous = previous & records[l];


            }
        }


    }

    // Driver code
    public static void main(String[] args) {
        char set[] = {'a', 'b', 'c'};
        printSubsets ();
    }
}