package com.technocrat.hackerrank.algo;

import com.technocrat.FasterScanner;

// 6 3 1
public class DrawingBook {

    public static void main(String[] args) {
        FasterScanner fasterScanner = new FasterScanner ();

        double noOfPages = fasterScanner.nextInt ();
        if (noOfPages % 2 == 0)
            noOfPages++;
        double pageNumber = fasterScanner.nextInt ();

        double middlePage = Math.ceil ( (noOfPages - 1) / 2 );
        double pageNumber_;
        if (pageNumber <= middlePage) {
            pageNumber_ = pageNumber;
        } else {
            pageNumber_ = noOfPages - pageNumber;
        }
        int result = (int) Math.floor ( pageNumber_ / 2 );
        System.out.println ( (int) Math.min ( noOfPages / 2, (noOfPages - pageNumber) / 2 ) );

    }
}
