package com.technocrat.hackerrank.warmup;

public class ThreadPrint100 {

    private static int counter = 1;
    private static int breakingPoint = 100;

    static ThreadPrint100 t1 = new ThreadPrint100 ();

    public static void main(String[] args) {
        Printer p1 = new Printer ( new int[]{1, 6}, "Thread 1" );

        Printer p2 = new Printer ( new int[]{2, 7}, "Thread 2" );

        Printer p3 = new Printer ( new int[]{3, 8}, "Thread 3" );

        Printer p4 = new Printer ( new int[]{4, 9}, "Thread 4" );

        Printer p5 = new Printer ( new int[]{5, 0}, "Thread 5" );
        p1.setP ( p2 );
        p2.setP ( p3 );
        p3.setP ( p4 );
        p4.setP ( p5 );
        p5.setP ( p1 );


        p1.start ();
        p2.start ();
        p3.start ();
        p4.start ();
        p5.start ();
    }

    static class Printer extends Thread {

        private int lastDight[];
        private String name;
        private Printer p;

        public void setP(Printer p) {
            this.p = p;
        }

        public Printer(int[] lastDight, String name) {
            this.lastDight = lastDight;
            this.name = name;

            setDaemon ( false );
        }


        @Override
        public void run() {
            while (true) {
                synchronized (t1) {
                    int lastDigit = counter % 10;
                    boolean wait = true;
                    for (int l : lastDight) {
                        if (l == lastDigit) {
                            if (counter <= breakingPoint)
                                System.out.println ( name + " : " + counter++ );
                            t1.notifyAll ();
                        }
                    }
                    if (counter < breakingPoint)
                        try {
                            t1.wait ();
                        } catch (InterruptedException e) {
                            e.printStackTrace ();
                        }
                    else
                        break;
                }
            }

        }
    }


}
