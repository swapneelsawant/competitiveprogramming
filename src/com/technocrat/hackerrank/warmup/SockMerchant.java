package com.technocrat.hackerrank.warmup;

import com.technocrat.FasterScanner;

public class SockMerchant {

    public static void main(String[] args) {
        FasterScanner fasterScanner = new FasterScanner ();

        byte noOfTestCases = (byte) fasterScanner.nextInt ();
        byte[] socks = new byte[101];
        int pair = 0;
        for (int i = 0; i < noOfTestCases; i++) {
            byte sockInd = (byte) fasterScanner.nextInt ();
            byte sockCounter = socks[sockInd];
            if (sockCounter == 0)
                socks[sockInd] = 1;
            else if (sockCounter == 1) {
                socks[sockInd] = 0;
                pair++;
            }
        }
        System.out.println ( pair );
    }
}
