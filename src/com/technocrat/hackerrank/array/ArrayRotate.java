package com.technocrat.hackerrank.array;

import com.technocrat.FasterScanner;

public class ArrayRotate {
    /**
     * 5 4
     * 1 2 3 4 5
     *
     * @param args
     */
    public static void main(String[] args) {
        FasterScanner fasterScanner = new FasterScanner ();

        int noOfArray = fasterScanner.nextInt ();
        int rotation = fasterScanner.nextInt ();
        int[] last = new int[rotation];

        int j = 0;

        StringBuilder sb = new StringBuilder ();
        for (int i = 0; i < noOfArray; i++) {
            int record = fasterScanner.nextInt ();
            if (i + 1 <= rotation)
                last[j++] = record;
            else {
                sb.append ( record );
                sb.append ( " " );
            }
        }
        j = 0;
        for (; j < last.length; j++) {
            sb.append ( last[j] );
            sb.append ( " " );
        }

        System.out.println ( sb.subSequence ( 0, sb.length () - 1 ) );
    }
}
