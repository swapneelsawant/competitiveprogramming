package com.technocrat.practice;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class PrintParalell {

    private static ReentrantLock lock = new ReentrantLock ();
    private static Condition condition = lock.newCondition ();
    private static int count = 1;

    public static void main(String[] args) {
        new Threads ( true ).start ();
        new Threads ( false ).start ();
    }


    private static class Threads extends Thread {
        private final boolean isEven;

        public Threads(boolean isEven) {
            this.isEven = isEven;
        }

        public void run() {
            while (count < 12) {
                try {
                    lock.lock ();
                    if (isEven) {
                        if (count % 2 == 0) {
                            System.out.println ( count++ );

                        }
                        condition.signalAll ();
                        try {
                            condition.await ();
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace ();
                        }
                    } else {
                        print ( count % 2 != 0 );
                    }
                } finally {
                    lock.unlock ();
                }
            }
        }

        private void print(boolean s) {
            if (s) {
                System.out.println ( count++ );

            }
            condition.signalAll ();
            try {
                condition.await ();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace ();
            }
        }
    }


}
