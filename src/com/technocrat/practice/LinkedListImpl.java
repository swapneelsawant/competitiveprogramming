package com.technocrat.practice;

public class LinkedListImpl {

    private Node root = null;
    private Node current = null;

    public static void main(String[] args) {
        LinkedListImpl linkedListImpl = new LinkedListImpl ();
        for (int i = 0; i < 5; i++) {
            linkedListImpl.add ( i );
        }
        Node slow = null;
        Node fast = slow = linkedListImpl.root.next;

        int counter = 1;
        while (fast != null) {
            fast = fast.next;
            if (counter++ % 2 == 0) {
                slow = slow.next;
            }
        }

        System.out.println ( slow.data );

    }

    private void add(Object data) {
        if (root == null) {
            root = new Node ();

            current = new Node ();
            current.data = data;
            root.next = current;
        } else {
            current.next = new Node ();
            current.next.data = data;
            current = current.next;
        }
    }


    @Override
    public String toString() {
        return "LinkedListImpl [root=" + root + ", current=" + current + "]";
    }

    private class Node {
        private Node next;
        private Object data;

        @Override
        public String toString() {
            return "Node [next=" + next + ", data=" + data + "]";
        }


    }

}

