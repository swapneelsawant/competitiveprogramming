package com.technocrat.practice;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

public class FutureTest {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newSingleThreadExecutor ();

        Set<Callable<String>> callables = new HashSet<Callable<String>> ();

        callables.add ( new Callable<String> () {
            public String call() throws Exception {
                return "Task 1";
            }
        } );
        callables.add ( new Callable<String> () {
            public String call() throws Exception {
                return "Task 2";
            }
        } );
        callables.add ( new Callable<String> () {
            public String call() throws Exception {
                return "Task 3";
            }
        } );

        List<Future<String>> futures = executorService.invokeAll ( callables );

        for (Future<String> future : futures) {
            System.out.println ( "future.get = " + future.get () );
        }

        executorService.shutdown ();

    }

    static class CallableMe implements Callable<String> {
        public String call() {
            if (true)
                throw new RuntimeException ();
            return "123098";
        }
    }
}
