package com.technocrat.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StringCount {

    public static void main(String[] args) {
        Scanner sc = new Scanner ( System.in );
        int testCases = sc.nextInt ();
        for (int i = 0; i < testCases; i++) {
            String test = sc.next ();
            char[] array = test.toCharArray ();
            List<List<Integer>> output = new ArrayList<> ( 26 );
            for (int j = 0; j < 26; j++) {
                output.add ( null );
            }
            for (int j = 0; j < array.length; j++) {
                int ch = array[j] - 97;
                List<Integer> list = output.get ( ch );
                if (list == null) {
                    list = new ArrayList<> ();
                    output.add ( ch, list );
                    list.add ( 0 );
                }
                list.add ( j );
            }

        }
    }

}
