package com.technocrat.practice.array;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MostRepeatedDigit {

    public static void main(String[] args) {

        Scanner s = new Scanner ( System.in );
        int N = s.nextInt ();
        Map<Integer, Integer> count = new HashMap<> ();
        int maxCounter = 0;
        int maxNumber = -1;
        for (int i = 0; i < N; i++) {
            int n = s.nextInt ();
            Integer j = count.get ( n );
            if (j == null) {
                j = 0;
            }
            j++;
            if (maxCounter < j) {
                maxCounter = j;
                maxNumber = n;
            } else if (maxCounter == j && maxNumber > n) {
                maxCounter = j;
                maxNumber = n;
            }
            count.put ( n, j );
        }
        System.out.println ( maxNumber );
    }

}
