package com.technocrat.practice;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Latch {

    public static void main(String[] args) {
        CyclicBarrier barrier = new CyclicBarrier ( 2 );

        ExecutorService stealingPool = Executors.newCachedThreadPool ();
        MyTread myTread = new MyTread ();
        myTread.barrier = barrier;
        stealingPool.submit ( myTread );
        myTread = new MyTread ();
        myTread.barrier = barrier;
        stealingPool.submit ( myTread );

        synchronized (myTread) {
            try {
                new Latch ().wait ( 1000 );
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace ();
            }
        }

    }

    static class MyTread implements Runnable {

        private CyclicBarrier barrier;

        @Override
        public void run() {

            try {
                barrier.await ();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace ();
            } catch (BrokenBarrierException e) {
                // TODO Auto-generated catch block
                e.printStackTrace ();
            }
            System.out.println ( "released" );

        }

    }

}
