package com.technocrat.hackerearth.array;


import com.technocrat.FasterScanner;

public class PowerofTwo {

    public static void main(String[] args) {

        FasterScanner fasterScanner = new FasterScanner ();

        int noOfTestCases = fasterScanner.nextInt ();

        outer:
        for (int i = 0; i < noOfTestCases; i++) {
            int noOfRecords = fasterScanner.nextInt ();
            int[] records = new int[noOfRecords];

            for (int j = 0; j < noOfRecords; j++) {
                records[j] = fasterScanner.nextInt ();
            }

            int n = records.length;

            // Run a loop for printing all 2^n
            // subsets one by one

            for (int k = 0; k < 1 << n; k++) {
                int previous = -1;
                // Print current subset
                for (int l = 0; l < n; l++)

                    // (1<<j) is a number with jth bit 1
                    // so when we 'and' them with the
                    // subset number we get which numbers
                    // are present in the subset and which
                    // are not
                    if ((k & (1 << l)) > 0) {
                        if (previous != -1)
                            previous = previous & records[l];
                        else
                            previous = records[l];

                        if (isPowerOfTwo ( previous )) {
                            System.out.println ( "YES" );
                            continue outer;

                        }
                    }


            }
            System.out.println ( "NO" );
        }


    }

    static boolean isPowerOfTwo(int x) {
      /* First x in the below expression is
        for the case when x is 0 */
        return x != 0 && ((x & (x - 1)) == 0);

    }

}
