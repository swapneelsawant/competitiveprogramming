package com.technocrat.hackerearth.array;

import com.technocrat.FasterScanner;

import java.util.Arrays;
import java.util.Comparator;

public class TowerofHanoi {
    /**
     * 2
     * 3
     * 4 3
     * 1 4
     * 3 2
     * 5
     * 5 6
     * 4 3
     * 1 2
     * 7 5
     * 3 4
     * Output
     * 5
     * 12
     *
     * @param args
     */
    public static void main(String[] args) {

        FasterScanner fasterScanner = new FasterScanner ();

        int noOfTestCases = fasterScanner.nextInt ();
        StringBuilder sb = new StringBuilder ();
        for (int i = 0; i < noOfTestCases; i++) {
            int noOfRecords = fasterScanner.nextInt ();
            long[][] disk = new long[noOfRecords][2];

            for (int j = 0; j < noOfRecords; j++) {
                long radius = fasterScanner.nextLong ();
                long height = fasterScanner.nextLong ();
                disk[j][0] = radius;
                disk[j][1] = height;

            }

            Arrays.sort ( disk, new Comparator<long[]> () {

                @Override
                // Compare values according to columns
                public int compare(final long[] entry1,
                                   final long[] entry2) {
                    int c = Long.compare ( entry1[0], entry2[0] );
                    if (c == 0)
                        return -1 * Long.compare ( entry1[1], entry2[1] );
                    return -1 * Long.compare ( entry1[0], entry2[0] );
                }
            } );
            long totalHeight = disk[0][1];
            long previousHeight = disk[0][1];
            for (int j = 1; j < noOfRecords - 1; j++) {

                long height = disk[j][1];
                if (previousHeight > height) {
                    totalHeight += height;
                    continue;
                }

                int k = j - 1;
                long totalHeightSubSet = previousHeight;
                while (k >= 0) {
                    long h = disk[k][1];
                    if (h > height && totalHeightSubSet < height) {
                        totalHeight = totalHeight - totalHeightSubSet + height;

                        break;
                    } else {
                        totalHeightSubSet += h;
                    }
                    if (totalHeightSubSet > height)
                        break;
                    k--;
                }


            }
            sb.append ( totalHeight );
            sb.append ( "\n" );

        }
        System.out.print ( sb.toString () );
    }
}
