package com.technocrat.hackerearth.array;

import com.technocrat.FasterScanner;

public class LongAtmQueue {
    public static void main(String args[]) throws Exception {
        FasterScanner fasterScanner = new FasterScanner ();

        int queueSize = fasterScanner.nextInt ();

        if (queueSize == 0) {
            System.out.println ( 0 );
            return;
        }

        int numberOfGroups = 1;

        if (queueSize == 1) {
            System.out.println ( numberOfGroups );
            return;
        }

        int heightOfPrevPerson = fasterScanner.nextInt ();
        int heightOfCurrentPerson;
        for (int i = 1; i < queueSize; i++) {
            heightOfCurrentPerson = fasterScanner.nextInt ();
            if (heightOfPrevPerson > heightOfCurrentPerson)
                numberOfGroups++;
            heightOfPrevPerson = heightOfCurrentPerson;
        }
        System.out.println ( numberOfGroups );
    }
}
